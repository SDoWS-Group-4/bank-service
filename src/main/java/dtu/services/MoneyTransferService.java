package dtu.services;

import dtu.models.CorrelationId;
import dtu.models.Payment;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.UUID;

public class MoneyTransferService {

	MessageQueue queue;
	BankService bank = new BankServiceService().getBankServicePort();

	public MoneyTransferService(MessageQueue q) {
		this.queue = q;
        this.queue.addHandler("TransferMoneyRequested", this::handleTransferMoneyRequested);
	}

	public void handleTransferMoneyRequested(Event ev) {
		String accountId;
		Event event;

		var payment = ev.getArgument(0, Payment.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		
		try {
			bank.transferMoneyFromTo(
			payment.getSender(),
			payment.getReceiver(),
			new BigDecimal("" + payment.getAmount()),
			"DTUPay money transfer");
			event = new Event("MoneyTransfered", new Object[] {correlationId});
		} catch (BankServiceException_Exception e) {
			event = new Event("MoneyNotTransfered", new Object[] {new Exception(e.getMessage()), correlationId});
		}
		queue.publish(event);
	}
}
