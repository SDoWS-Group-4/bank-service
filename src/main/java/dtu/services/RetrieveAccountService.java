package dtu.services;

import dtu.models.CorrelationId;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import messaging.Event;
import messaging.MessageQueue;

import java.util.HashMap;

public class RetrieveAccountService {

	MessageQueue queue;
	BankService bank = new BankServiceService().getBankServicePort();

	public RetrieveAccountService(MessageQueue q) {
		this.queue = q;
		this.queue.addHandler("RetrieveAccountRequested", this::handleAccountRegistrationRequested);
	}
	
	public void handleAccountRegistrationRequested(Event ev) {
		Event event;

		var accountId = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		
		try {

			Account ac = bank.getAccount(accountId);

			var data = new HashMap<String, String>();
			data.put("ID",			ac.getId());
			data.put("CPR", 		ac.getUser().getCprNumber());
			data.put("firstname", 	ac.getUser().getFirstName());
			data.put("lastname", 	ac.getUser().getLastName());
			data.put("balance", 	ac.getBalance().toPlainString());			

			event = new Event("AccountRetrieved", new Object[] { data, correlationId });
		} catch (BankServiceException_Exception e) {
			event = new Event("AccountNotRetrieved", new Object[] { e, correlationId });
		}

		queue.publish(event);
	}
}
