package dtu.services;

import dtu.models.CorrelationId;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;

public class RetireAccountService {

	MessageQueue queue;
	BankService bank = new BankServiceService().getBankServicePort();

	public RetireAccountService(MessageQueue q) {
		this.queue = q;
		this.queue.addHandler("RetireAccountRequested", this::handleAccountRegistrationRequested);
	}

	public void handleAccountRegistrationRequested(Event ev) {
		Event event;

		var accountId = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		try {
			bank.retireAccount(accountId);
			event = new Event("AccountRetired", new Object[] {accountId, correlationId});
		} catch (BankServiceException_Exception e) {
			event = new Event("AccountNotRetired", new Object[] {e, correlationId});
		}

		queue.publish(event);
	}
}
