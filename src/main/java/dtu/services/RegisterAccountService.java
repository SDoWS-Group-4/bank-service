package dtu.services;

import dtu.models.CorrelationId;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;

public class RegisterAccountService {

	MessageQueue queue;
	BankService bank = new BankServiceService().getBankServicePort();

	public RegisterAccountService(MessageQueue q) {
		this.queue = q;
		this.queue.addHandler("RegisterAccountRequested", this::handleAccountRegistrationRequested);
	}

	public void handleAccountRegistrationRequested(Event ev) {
		String accountId;
		Event event;

		var user = ev.getArgument(0, User.class);
		var amount = ev.getArgument(1, BigDecimal.class);
		var correlationId = ev.getArgument(2, CorrelationId.class);
		try {
			accountId = bank.createAccountWithBalance(user, amount);
			event = new Event("AccountRegistered", new Object[] {accountId, correlationId});
		} catch (BankServiceException_Exception e) {
			Exception ex = new Exception(e.getMessage());
			event = new Event("AccountNotRegistered", new Object[] {ex, correlationId});
		}
		queue.publish(event);
	}
}
