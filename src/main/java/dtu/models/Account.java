package dtu.models;


import lombok.Getter;
import lombok.Setter;
import lombok.Data;
@Data
public class Account {

    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private String CPR;
    @Getter @Setter private String bankAccountID;

    public Account() {
    }

}
