package dtu;

import dtu.services.MoneyTransferService;
import dtu.services.RegisterAccountService;
import dtu.services.RetireAccountService;
import dtu.services.RetrieveAccountService;

import dtu.ws.fastmoney.RetireAccount;
import messaging.implementations.RabbitMqQueue;

public class StartUp {
	public static void main(String[] args) throws Exception {
		new StartUp().startUp();
	}

	private void startUp() throws Exception {
		System.out.println("startup");
		var mq = new RabbitMqQueue("rabbitmq");
		new MoneyTransferService(mq);
		new RegisterAccountService(mq);
		new RetireAccountService(mq);
		new RetrieveAccountService(mq);
//		new CustomerIdService(mq);
	}
}
