#!/bin/bash

set -e

# pushd is the same as cd but stays there.
# go to Service Directory
Service=bank-service

## Clean directory 
mvn clean
## Test application
mvn test
# package build
# mvn clean package DOES NOT WORK!
mvn clean install

## build dockerfile
docker build . -t $Service
# clean up
docker system prune -f
#docker run -i --rm -p 8080 &

if [ "$(docker ps -q -f name=rabbitmq)" ]; then

  docker-compose -f ./../system/docker-compose.yaml up -d --no-deps $Service
  else
    echo "RabbitMQ not active"
    # exit 1
fi
##save server process variable id
##server_pid=$!
##if an error occurs or at end of script, kill server
#trap 'kill $server_pid' err exit

# Go back to main folder

